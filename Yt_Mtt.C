#define Yt_Mtt_cxx
#include "Yt_Mtt.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>

void Yt_Mtt::Loop()
{
 	#ifdef __CINT__
		gROOT->LoadMacro("AtlasLabels.C");
		gROOT->LoadMacro("AtlasUtils.C");
        gROOT->LoadMacro("AtlasStyle.C");
	#endif

   SetAtlasStyle();
  // TStyle *atlasStyle = new TStyle("ATLAS","Atlas style");
   if (fChain == 0) return;
   

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   TH1F * h_Mbbll =new TH1F("Mbbll","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * h_Mbbll_0 =new TH1F("Mbbll_0","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * h_Mbbll_4 =new TH1F("Mbbll_4","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * h_Mbbll_9 =new TH1F("Mbbll_9","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * h_Mbbll_16 =new TH1F("Mbbll_16","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * ratio_h_Mbbll_0 =new TH1F("ratio_h_Mbbll_0","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * ratio_h_Mbbll_4 =new TH1F("ratio_h_Mbbll_4","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * ratio_h_Mbbll_9 =new TH1F("ratio_h_Mbbll_9","Invariant mass of #it{bbll}",30,370.,800);
   TH1F * ratio_h_Mbbll_16 =new TH1F("ratio_h_Mbbll_16","Invariant mass of #it{bbll}",30,370.,800);
   
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
    //std::cout<<"Ciao"<<std::endl;
    if(t_met_met>0){
    
	    h_Mbbll->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m*t_weight_hathor_ytsq1)/1000);
	    h_Mbbll_0->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq0/1000);
	    h_Mbbll_4->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq4/1000);
	    h_Mbbll_9->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq9/1000);
	    h_Mbbll_16->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq16/1000);
	    ratio_h_Mbbll_0->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq0/1000);
	    ratio_h_Mbbll_4->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq4/1000);
	    ratio_h_Mbbll_9->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq9/1000);
	    ratio_h_Mbbll_16->Fill((t_truth_MC_ttbar_afterFSR_beforeDecay_m)*t_weight_hathor_ytsq16/1000);
	    //h_Mbbll->Fill((e.M()+mu.M()+bjet1.M()+bjet2.M())/1000.);
}

  }
       std::cout<<"Ciao"<<std::endl;
   TH1 * g=new TH1F("g","Gost for legend",30,370.,800);;
   g->SetLineColor(0);
   g->SetMarkerColor(0);
   g->SetLineWidth(0);
   //g->SetMarkerStyle(0);
   h_Mbbll->GetXaxis()->SetTitle("M_{#it{tt}} [GeV]");
   h_Mbbll->GetYaxis()->SetTitle("Counts");
   //h_Mbbll->SetMaximum(std::max(h_Mbbll->GetMaximum(),h_Mbbll_9->GetMaximum(),h_Mbbll_16->GetMaximum(),h_Mbbll_4->GetMaximum()));
   h_Mbbll->SetMaximum(h_Mbbll_4->GetMaximum());
   h_Mbbll_0->SetLineColor(2);
   h_Mbbll_4->SetLineColor(3);
   h_Mbbll_9->SetLineColor(4);
   h_Mbbll_16->SetLineColor(6);
   ratio_h_Mbbll_0->SetLineColor(2);
   ratio_h_Mbbll_4->SetLineColor(3);
   ratio_h_Mbbll_9->SetLineColor(4);
   ratio_h_Mbbll_16->SetLineColor(6);
   h_Mbbll_0->SetLineWidth(2);
   h_Mbbll_4->SetLineWidth(2);
   h_Mbbll_9->SetLineWidth(2);
   TFile saving ("M_tt_out_77.root","RECREATE");
   h_Mbbll_0->Write();
   h_Mbbll_4->Write();
   h_Mbbll_9->Write();
   h_Mbbll->Write();
   saving.Close();
   
   
   ratio_h_Mbbll_0->Divide(h_Mbbll);
   ratio_h_Mbbll_4->Divide(h_Mbbll);
   ratio_h_Mbbll_9->Divide(h_Mbbll);
   ratio_h_Mbbll_16->Divide(h_Mbbll);
   
   
   
   
   TCanvas *cs = new TCanvas("cs","cs",200, 10, 700, 750);
   TPad *pad1 = new TPad("pad1" ,"pad1" , 0.02, 0.33, 1, 1);
   pad1->SetBottomMargin(0.03);
    pad1->SetTopMargin(0.13);
    pad1->SetLeftMargin(0.15);
    pad1->SetRightMargin(0.09);
    pad1->Draw();
    pad1->cd();
std::cout<<"before drawing"<<std::endl;
    //cs->cd(3)->SetRightMargin(0.18);
      	h_Mbbll->Draw("");
       	h_Mbbll_0->Draw("SAME");
        h_Mbbll_4->Draw("SAME");
        h_Mbbll_9->Draw("SAME");
        h_Mbbll_16->Draw("SAME");
       	g->Draw("SAME");
    TLegend *leg1 = new TLegend(0.9, 0.65, 0.5, 0.83); //0.9, 0.65, 0.9, 0.83
    leg1->SetBorderSize(0);
	leg1->AddEntry(g,"#bf{#it{ATLAS}} Internal","lp");
	leg1->AddEntry(g, "#sqrt{s}= 13 TeV","lp");
    leg1->AddEntry(g, "t#bar{t}, leptonic, #epsilon_{#it{b}}=77%","lp");
    leg1->Draw();
    TLegend *leg = new TLegend(); //0.9, 0.65, 0.9, 0.83
    leg->SetBorderSize(0);
	leg->AddEntry(h_Mbbll,"y_{t}^{2} = 1","l");
	leg->AddEntry(h_Mbbll_0, "y_{t}^{2} = 0","l");
	leg->AddEntry(h_Mbbll_4, "y_{t}^{2} = 4","l");
	leg->AddEntry(h_Mbbll_9, "y_{t}^{2} = 9","l");
	leg->AddEntry(h_Mbbll_16, "y_{t}^{2} = 16","l");
    leg->Draw();
    //cs->cd();
    
       TPad *pad2 = new TPad("pad2" ,"pad2" ,  0.02, 0.05, 1, 0.35);
   pad2->SetBottomMargin(0.3);
    pad2->SetTopMargin(0.02);
    pad2->SetLeftMargin(0.15);
    pad2->SetRightMargin(0.09);
    pad2->Draw();
    pad2->cd();
    

       	ratio_h_Mbbll_0->Draw("");
        ratio_h_Mbbll_4->Draw("SAME");
        ratio_h_Mbbll_9->Draw("SAME");
        ratio_h_Mbbll_16->Draw("SAME");
    //cs->cd();
    
    cs->SaveAs("Ratio_M_tt_plot_77_Yt_sensitivity.png");
std::cout<<"end"<<std::endl;

}
