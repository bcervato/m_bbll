//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jun 15 08:19:54 2023 by ROOT version 6.24/02
// from TTree nominal/
// found on file: OUT_Yt_DILEPTON.root
//////////////////////////////////////////////////////////

#ifndef Yt_noKLFitter_h
#define Yt_noKLFitter_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class Yt_noKLFitter {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         xsec_feff_kfac;
   Float_t         t_weight_mc;
   Float_t         t_weight_pileup;
   Float_t         t_weight_leptonSF;
   Float_t         t_weight_globalLeptonTriggerSF;
   Float_t         t_weight_oldTriggerSF;
   Float_t         t_weight_bTagSF_DL1r_77;
   Float_t         t_weight_jvt;
   vector<string>  *t_names_mc_generator_weights;
   vector<float>   *temp_totalEventsWeighted_mc_generator_weights;
   Float_t         t_totalEventsWeighted;
   vector<float>   *t_mc_generator_weights;
   Float_t         t_weight_pileup_UP;
   Float_t         t_weight_pileup_DOWN;
   Float_t         t_weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         t_weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         t_weight_leptonSF_EL_SF_Reco_UP;
   Float_t         t_weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         t_weight_leptonSF_EL_SF_ID_UP;
   Float_t         t_weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         t_weight_leptonSF_EL_SF_Isol_UP;
   Float_t         t_weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         t_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         t_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         t_weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         t_weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         t_weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         t_weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         t_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         t_weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         t_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         t_weight_globalLeptonTriggerSF_EL_Trigger_UP;
   Float_t         t_weight_globalLeptonTriggerSF_EL_Trigger_DOWN;
   Float_t         t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;
   Float_t         t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;
   Float_t         t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         t_weight_oldTriggerSF_EL_Trigger_UP;
   Float_t         t_weight_oldTriggerSF_EL_Trigger_DOWN;
   Float_t         t_weight_oldTriggerSF_MU_Trigger_STAT_UP;
   Float_t         t_weight_oldTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         t_weight_oldTriggerSF_MU_Trigger_SYST_UP;
   Float_t         t_weight_oldTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         t_weight_jvt_UP;
   Float_t         t_weight_jvt_DOWN;
   vector<float>   *t_weight_bTagSF_DL1r_77_eigenvars_B_up;
   vector<float>   *t_weight_bTagSF_DL1r_77_eigenvars_C_up;
   vector<float>   *t_weight_bTagSF_DL1r_77_eigenvars_Light_up;
   vector<float>   *t_weight_bTagSF_DL1r_77_eigenvars_B_down;
   vector<float>   *t_weight_bTagSF_DL1r_77_eigenvars_C_down;
   vector<float>   *t_weight_bTagSF_DL1r_77_eigenvars_Light_down;
   Float_t         t_weight_bTagSF_DL1r_77_extrapolation_up;
   Float_t         t_weight_bTagSF_DL1r_77_extrapolation_down;
   Float_t         t_weight_bTagSF_DL1r_77_extrapolation_from_charm_up;
   Float_t         t_weight_bTagSF_DL1r_77_extrapolation_from_charm_down;
   Bool_t          t_el_trigger_pass;
   Bool_t          t_mu_trigger_pass;
   ULong64_t       t_eventNumber;
   UInt_t          t_runNumber;
   UInt_t          t_mcChannelNumber;
   Float_t         t_mu;
   Float_t         t_mu_actual;
   Int_t           t_n_el;
   Int_t           t_n_mu;
   vector<float>   *t_el_pt;
   vector<float>   *t_el_eta;
   vector<float>   *t_el_phi;
   vector<float>   *t_el_e;
   vector<float>   *t_mu_pt;
   vector<float>   *t_mu_eta;
   vector<float>   *t_mu_phi;
   vector<float>   *t_mu_e;
   vector<float>   *t_lep_d0sig;
   Int_t           t_Goodjet_num;
   Int_t           t_jet_num;
   Int_t           t_bjet_num;
   vector<float>   *t_jet_pt;
   vector<float>   *t_jet_eta;
   vector<float>   *t_jet_phi;
   vector<float>   *t_jet_m;
   vector<float>   *t_jet_jvt;
   vector<int>     *t_jet_isbtagged_DL1r_77;
   vector<float>   *t_jet_DL1r;
   Float_t         t_met_met;
   Float_t         t_met_phi;
   Int_t           t_ejets_DL1r_2015;
   Int_t           t_ejets_DL1r_2016;
   Int_t           t_ejets_DL1r_2017;
   Int_t           t_ejets_DL1r_2018;
   Int_t           t_mujets_DL1r_2015;
   Int_t           t_mujets_DL1r_2016;
   Int_t           t_mujets_DL1r_2017;
   Int_t           t_mujets_DL1r_2018;
   Int_t           t_ejets_particle;
   Int_t           t_mujets_particle;
   Float_t         t_reco_lj1_pt;
   Float_t         t_reco_lj1_eta;
   Float_t         t_reco_lj1_phi;
   Float_t         t_reco_lj1_e;
   Float_t         t_reco_lj2_pt;
   Float_t         t_reco_lj2_eta;
   Float_t         t_reco_lj2_phi;
   Float_t         t_reco_lj2_e;
   Float_t         t_reco_Whad_pt;
   Float_t         t_reco_Whad_eta;
   Float_t         t_reco_Whad_phi;
   Float_t         t_reco_Whad_m;
   Float_t         t_reco_bhad_pt;
   Float_t         t_reco_bhad_eta;
   Float_t         t_reco_bhad_phi;
   Float_t         t_reco_bhad_m;
   Float_t         t_reco_thad_pt;
   Float_t         t_reco_thad_eta;
   Float_t         t_reco_thad_phi;
   Float_t         t_reco_thad_e;
   Float_t         t_reco_thad_m;
   Float_t         t_reco_nu_pt;
   Float_t         t_reco_nu_eta;
   Float_t         t_reco_nu_phi;
   Float_t         t_reco_nu_e;
   Float_t         t_reco_nu_pt_z;
   Bool_t          t_delta_neg;
   Bool_t          t_delta_neg_resc;
   Bool_t          t_delta_neg_4;
   Bool_t          t_delta_neg_resc_4;
   Float_t         t_reco_lep_pt;
   Float_t         t_reco_lep_eta;
   Float_t         t_reco_lep_phi;
   Float_t         t_reco_lep_e;
   Float_t         t_reco_Wlep_pt;
   Float_t         t_reco_Wlep_eta;
   Float_t         t_reco_Wlep_phi;
   Float_t         t_reco_Wlep_m;
   Float_t         t_reco_Wlep_mT;
   Float_t         t_reco_Wlep_m_4vec;
   Float_t         t_reco_blep_pt;
   Float_t         t_reco_blep_eta;
   Float_t         t_reco_blep_phi;
   Float_t         t_reco_blep_m;
   Float_t         t_reco_tlep_pt;
   Float_t         t_reco_tlep_eta;
   Float_t         t_reco_tlep_phi;
   Float_t         t_reco_tlep_e;
   Float_t         t_reco_tlep_m;
   Float_t         t_reco_ttbar_pt;
   Float_t         t_reco_ttbar_eta;
   Float_t         t_reco_ttbar_phi;
   Float_t         t_reco_ttbar_e;
   Float_t         t_reco_ttbar_m;
   Float_t         t_reco_lj1_resc_pt;
   Float_t         t_reco_lj1_resc_eta;
   Float_t         t_reco_lj1_resc_phi;
   Float_t         t_reco_lj1_resc_e;
   Float_t         t_reco_lj2_resc_pt;
   Float_t         t_reco_lj2_resc_eta;
   Float_t         t_reco_lj2_resc_phi;
   Float_t         t_reco_lj2_resc_e;
   Float_t         t_reco_Whad_resc_pt;
   Float_t         t_reco_Whad_resc_eta;
   Float_t         t_reco_Whad_resc_phi;
   Float_t         t_reco_Whad_resc_m;
   Float_t         t_reco_bhad_resc_pt;
   Float_t         t_reco_bhad_resc_eta;
   Float_t         t_reco_bhad_resc_phi;
   Float_t         t_reco_bhad_resc_m;
   Float_t         t_reco_thad_resc_pt;
   Float_t         t_reco_thad_resc_eta;
   Float_t         t_reco_thad_resc_phi;
   Float_t         t_reco_thad_resc_e;
   Float_t         t_reco_thad_resc_m;
   Float_t         t_reco_nu_resc_pt;
   Float_t         t_reco_nu_resc_eta;
   Float_t         t_reco_nu_resc_phi;
   Float_t         t_reco_nu_resc_e;
   Float_t         t_reco_lep_resc_pt;
   Float_t         t_reco_lep_resc_eta;
   Float_t         t_reco_lep_resc_phi;
   Float_t         t_reco_lep_resc_e;
   Float_t         t_reco_Wlep_resc_pt;
   Float_t         t_reco_Wlep_resc_eta;
   Float_t         t_reco_Wlep_resc_phi;
   Float_t         t_reco_Wlep_resc_m;
   Float_t         t_reco_Wlep_resc_mT;
   Float_t         t_reco_blep_resc_pt;
   Float_t         t_reco_blep_resc_eta;
   Float_t         t_reco_blep_resc_phi;
   Float_t         t_reco_blep_resc_m;
   Float_t         t_reco_tlep_resc_pt;
   Float_t         t_reco_tlep_resc_eta;
   Float_t         t_reco_tlep_resc_phi;
   Float_t         t_reco_tlep_resc_e;
   Float_t         t_reco_tlep_resc_m;
   Float_t         t_reco_ttbar_resc_pt;
   Float_t         t_reco_ttbar_resc_eta;
   Float_t         t_reco_ttbar_resc_phi;
   Float_t         t_reco_ttbar_resc_e;
   Float_t         t_reco_ttbar_resc_m;
   Float_t         t_HT_jets;
   Float_t         t_ST_jets_ETmiss_lep;
   vector<float>   *truth_mc_pow_pt;
   vector<float>   *truth_mc_pow_eta;
   vector<float>   *truth_mc_pow_phi;
   vector<float>   *truth_mc_pow_e;
   vector<int>     *truth_mc_pow_pdgId;
   vector<int>     *truth_mc_pow_status;
   vector<int>     *truth_PDFinfo_PDGID1;
   vector<int>     *truth_PDFinfo_PDGID2;
   vector<float>   *t_matched_reco_PARTON_dRmin;
   vector<int>     *t_matched_reco_PARTON_recoJetIndex;
   vector<int>     *t_matched_reco_PARTON_PARTONJetIndex;
   Int_t           t_PARTONmatchedJets_TotalNum;
   vector<int>     *t_all_jet_reco_PARTON_Index_eachRecoJet;
   Int_t           t_isPerfectMatch_PARTONLevel;
   Float_t         t_partonLev_nu_met;
   Float_t         t_partonLev_nu_eta;
   Float_t         t_partonLev_nu_phi;
   Float_t         t_partonLev_nu_m;
   Int_t           t_reco_Wjet1_ind;
   Int_t           t_reco_Wjet2_ind;
   Int_t           t_reco_bhad_ind;
   Int_t           t_reco_blep_ind;
   vector<int>     *t_PARTON_jetInd_dRmin;
   vector<float>   *t_PARTON_dRmin;
   Int_t           t_noPARTONMatch;
   Float_t         t_truth_MC_ttbar_afterFSR_beforeDecay_m;
   Double_t        t_weight_hathor_ytsqm9;
   Double_t        t_weight_hathor_ytsqm4;
   Double_t        t_weight_hathor_ytsqm1;
   Double_t        t_weight_hathor_ytsq0;
   Double_t        t_weight_hathor_ytsq1;
   Double_t        t_weight_hathor_ytsq4;
   Double_t        t_weight_hathor_ytsq9;
   Double_t        t_weight_hathor_ytsq16;

   // List of branches
   TBranch        *b_xsec_feff_kfac;   //!
   TBranch        *b_t_weight_mc;   //!
   TBranch        *b_t_weight_pileup;   //!
   TBranch        *b_t_weight_leptonSF;   //!
   TBranch        *b_t_weight_globalLeptonTriggerSF;   //!
   TBranch        *b_t_weight_oldTriggerSF;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77;   //!
   TBranch        *b_t_weight_jvt;   //!
   TBranch        *b_t_names_mc_generator_weights;   //!
   TBranch        *b_temp_totalEventsWeighted_mc_generator_weights;   //!
   TBranch        *b_t_totalEventsWeighted;   //!
   TBranch        *b_t_mc_generator_weights;   //!
   TBranch        *b_t_weight_pileup_UP;   //!
   TBranch        *b_t_weight_pileup_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_t_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_t_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_t_weight_globalLeptonTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_t_weight_globalLeptonTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_t_weight_oldTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_t_weight_oldTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_t_weight_oldTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_t_weight_oldTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_t_weight_oldTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_t_weight_oldTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_t_weight_jvt_UP;   //!
   TBranch        *b_t_weight_jvt_DOWN;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_eigenvars_B_up;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_eigenvars_C_up;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_eigenvars_Light_up;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_eigenvars_B_down;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_eigenvars_C_down;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_eigenvars_Light_down;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_extrapolation_up;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_extrapolation_down;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_extrapolation_from_charm_up;   //!
   TBranch        *b_t_weight_bTagSF_DL1r_77_extrapolation_from_charm_down;   //!
   TBranch        *b_t_el_trigger_pass;   //!
   TBranch        *b_t_mu_trigger_pass;   //!
   TBranch        *b_t_eventNumber;   //!
   TBranch        *b_t_runNumber;   //!
   TBranch        *b_t_mcChannelNumber;   //!
   TBranch        *b_t_mu;   //!
   TBranch        *b_t_mu_actual;   //!
   TBranch        *b_t_n_el;   //!
   TBranch        *b_t_n_mu;   //!
   TBranch        *b_t_el_pt;   //!
   TBranch        *b_t_el_eta;   //!
   TBranch        *b_t_el_phi;   //!
   TBranch        *b_t_el_e;   //!
   TBranch        *b_t_mu_pt;   //!
   TBranch        *b_t_mu_eta;   //!
   TBranch        *b_t_mu_phi;   //!
   TBranch        *b_t_mu_e;   //!
   TBranch        *b_t_lep_d0sig;   //!
   TBranch        *b_t_Goodjet_num;   //!
   TBranch        *b_t_jet_num;   //!
   TBranch        *b_t_bjet_num;   //!
   TBranch        *b_t_jet_pt;   //!
   TBranch        *b_t_jet_eta;   //!
   TBranch        *b_t_jet_phi;   //!
   TBranch        *b_t_jet_m;   //!
   TBranch        *b_t_jet_jvt;   //!
   TBranch        *b_t_jet_isbtagged_DL1r_77;   //!
   TBranch        *b_t_jet_DL1r;   //!
   TBranch        *b_t_met_met;   //!
   TBranch        *b_t_met_phi;   //!
   TBranch        *b_t_ejets_DL1r_2015;   //!
   TBranch        *b_t_ejets_DL1r_2016;   //!
   TBranch        *b_t_ejets_DL1r_2017;   //!
   TBranch        *b_t_ejets_DL1r_2018;   //!
   TBranch        *b_t_mujets_DL1r_2015;   //!
   TBranch        *b_t_mujets_DL1r_2016;   //!
   TBranch        *b_t_mujets_DL1r_2017;   //!
   TBranch        *b_t_mujets_DL1r_2018;   //!
   TBranch        *b_t_ejets_particle;   //!
   TBranch        *b_t_mujets_particle;   //!
   TBranch        *b_t_reco_lj1_pt;   //!
   TBranch        *b_t_reco_lj1_eta;   //!
   TBranch        *b_t_reco_lj1_phi;   //!
   TBranch        *b_t_reco_lj1_e;   //!
   TBranch        *b_t_reco_lj2_pt;   //!
   TBranch        *b_t_reco_lj2_eta;   //!
   TBranch        *b_t_reco_lj2_phi;   //!
   TBranch        *b_t_reco_lj2_e;   //!
   TBranch        *b_t_reco_Whad_pt;   //!
   TBranch        *b_t_reco_Whad_eta;   //!
   TBranch        *b_t_reco_Whad_phi;   //!
   TBranch        *b_t_reco_Whad_m;   //!
   TBranch        *b_t_reco_bhad_pt;   //!
   TBranch        *b_t_reco_bhad_eta;   //!
   TBranch        *b_t_reco_bhad_phi;   //!
   TBranch        *b_t_reco_bhad_m;   //!
   TBranch        *b_t_reco_thad_pt;   //!
   TBranch        *b_t_reco_thad_eta;   //!
   TBranch        *b_t_reco_thad_phi;   //!
   TBranch        *b_t_reco_thad_e;   //!
   TBranch        *b_t_reco_thad_m;   //!
   TBranch        *b_t_reco_nu_pt;   //!
   TBranch        *b_t_reco_nu_eta;   //!
   TBranch        *b_t_reco_nu_phi;   //!
   TBranch        *b_t_reco_nu_e;   //!
   TBranch        *b_t_reco_nu_pt_z;   //!
   TBranch        *b_t_delta_neg;   //!
   TBranch        *b_t_delta_neg_resc;   //!
   TBranch        *b_t_delta_neg_4;   //!
   TBranch        *b_t_delta_neg_resc_4;   //!
   TBranch        *b_t_reco_lep_pt;   //!
   TBranch        *b_t_reco_lep_eta;   //!
   TBranch        *b_t_reco_lep_phi;   //!
   TBranch        *b_t_reco_lep_e;   //!
   TBranch        *b_t_reco_Wlep_pt;   //!
   TBranch        *b_t_reco_Wlep_eta;   //!
   TBranch        *b_t_reco_Wlep_phi;   //!
   TBranch        *b_t_reco_Wlep_m;   //!
   TBranch        *b_t_reco_Wlep_mT;   //!
   TBranch        *b_t_reco_Wlep_m_4vec;   //!
   TBranch        *b_t_reco_blep_pt;   //!
   TBranch        *b_t_reco_blep_eta;   //!
   TBranch        *b_t_reco_blep_phi;   //!
   TBranch        *b_t_reco_blep_m;   //!
   TBranch        *b_t_reco_tlep_pt;   //!
   TBranch        *b_t_reco_tlep_eta;   //!
   TBranch        *b_t_reco_tlep_phi;   //!
   TBranch        *b_t_reco_tlep_e;   //!
   TBranch        *b_t_reco_tlep_m;   //!
   TBranch        *b_t_reco_ttbar_pt;   //!
   TBranch        *b_t_reco_ttbar_eta;   //!
   TBranch        *b_t_reco_ttbar_phi;   //!
   TBranch        *b_t_reco_ttbar_e;   //!
   TBranch        *b_t_reco_ttbar_m;   //!
   TBranch        *b_t_reco_lj1_resc_pt;   //!
   TBranch        *b_t_reco_lj1_resc_eta;   //!
   TBranch        *b_t_reco_lj1_resc_phi;   //!
   TBranch        *b_t_reco_lj1_resc_e;   //!
   TBranch        *b_t_reco_lj2_resc_pt;   //!
   TBranch        *b_t_reco_lj2_resc_eta;   //!
   TBranch        *b_t_reco_lj2_resc_phi;   //!
   TBranch        *b_t_reco_lj2_resc_e;   //!
   TBranch        *b_t_reco_Whad_resc_pt;   //!
   TBranch        *b_t_reco_Whad_resc_eta;   //!
   TBranch        *b_t_reco_Whad_resc_phi;   //!
   TBranch        *b_t_reco_Whad_resc_m;   //!
   TBranch        *b_t_reco_bhad_resc_pt;   //!
   TBranch        *b_t_reco_bhad_resc_eta;   //!
   TBranch        *b_t_reco_bhad_resc_phi;   //!
   TBranch        *b_t_reco_bhad_resc_m;   //!
   TBranch        *b_t_reco_thad_resc_pt;   //!
   TBranch        *b_t_reco_thad_resc_eta;   //!
   TBranch        *b_t_reco_thad_resc_phi;   //!
   TBranch        *b_t_reco_thad_resc_e;   //!
   TBranch        *b_t_reco_thad_resc_m;   //!
   TBranch        *b_t_reco_nu_resc_pt;   //!
   TBranch        *b_t_reco_nu_resc_eta;   //!
   TBranch        *b_t_reco_nu_resc_phi;   //!
   TBranch        *b_t_reco_nu_resc_e;   //!
   TBranch        *b_t_reco_lep_resc_pt;   //!
   TBranch        *b_t_reco_lep_resc_eta;   //!
   TBranch        *b_t_reco_lep_resc_phi;   //!
   TBranch        *b_t_reco_lep_resc_e;   //!
   TBranch        *b_t_reco_Wlep_resc_pt;   //!
   TBranch        *b_t_reco_Wlep_resc_eta;   //!
   TBranch        *b_t_reco_Wlep_resc_phi;   //!
   TBranch        *b_t_reco_Wlep_resc_m;   //!
   TBranch        *b_t_reco_Wlep_resc_mT;   //!
   TBranch        *b_t_reco_blep_resc_pt;   //!
   TBranch        *b_t_reco_blep_resc_eta;   //!
   TBranch        *b_t_reco_blep_resc_phi;   //!
   TBranch        *b_t_reco_blep_resc_m;   //!
   TBranch        *b_t_reco_tlep_resc_pt;   //!
   TBranch        *b_t_reco_tlep_resc_eta;   //!
   TBranch        *b_t_reco_tlep_resc_phi;   //!
   TBranch        *b_t_reco_tlep_resc_e;   //!
   TBranch        *b_t_reco_tlep_resc_m;   //!
   TBranch        *b_t_reco_ttbar_resc_pt;   //!
   TBranch        *b_t_reco_ttbar_resc_eta;   //!
   TBranch        *b_t_reco_ttbar_resc_phi;   //!
   TBranch        *b_t_reco_ttbar_resc_e;   //!
   TBranch        *b_t_reco_ttbar_resc_m;   //!
   TBranch        *b_t_HT_jets;   //!
   TBranch        *b_t_ST_jets_ETmiss_lep;   //!
   TBranch        *b_truth_mc_pow_pt;   //!
   TBranch        *b_truth_mc_pow_eta;   //!
   TBranch        *b_truth_mc_pow_phi;   //!
   TBranch        *b_truth_mc_pow_e;   //!
   TBranch        *b_truth_mc_pow_pdgId;   //!
   TBranch        *b_truth_mc_pow_status;   //!
   TBranch        *b_truth_PDFinfo_PDGID1;   //!
   TBranch        *b_truth_PDFinfo_PDGID2;   //!
   TBranch        *b_t_matched_reco_PARTON_dRmin;   //!
   TBranch        *b_t_matched_reco_PARTON_recoJetIndex;   //!
   TBranch        *b_t_matched_reco_PARTON_PARTONJetIndex;   //!
   TBranch        *b_t_PARTONmatchedJets_TotalNum;   //!
   TBranch        *b_t_all_jet_reco_PARTON_Index_eachRecoJet;   //!
   TBranch        *b_t_isPerfectMatch_PARTONLevel;   //!
   TBranch        *b_t_partonLev_nu_met;   //!
   TBranch        *b_t_partonLev_nu_eta;   //!
   TBranch        *b_t_partonLev_nu_phi;   //!
   TBranch        *b_t_partonLev_nu_m;   //!
   TBranch        *b_t_reco_Wjet1_ind;   //!
   TBranch        *b_t_reco_Wjet2_ind;   //!
   TBranch        *b_t_reco_bhad_ind;   //!
   TBranch        *b_t_reco_blep_ind;   //!
   TBranch        *b_t_PARTON_jetInd_dRmin;   //!
   TBranch        *b_t_PARTON_dRmin;   //!
   TBranch        *b_t_noPARTONMatch;   //!
   TBranch        *b_t_truth_MC_ttbar_afterFSR_beforeDecay_m;   //!
   TBranch        *b_t_weight_hathor_ytsqm9;   //!
   TBranch        *b_t_weight_hathor_ytsqm4;   //!
   TBranch        *b_t_weight_hathor_ytsqm1;   //!
   TBranch        *b_t_weight_hathor_ytsq0;   //!
   TBranch        *b_t_weight_hathor_ytsq1;   //!
   TBranch        *b_t_weight_hathor_ytsq4;   //!
   TBranch        *b_t_weight_hathor_ytsq9;   //!
   TBranch        *b_t_weight_hathor_ytsq16;   //!

   Yt_noKLFitter(TTree *tree=0);
   virtual ~Yt_noKLFitter();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Yt_noKLFitter_cxx
Yt_noKLFitter::Yt_noKLFitter(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("OUT_Yt_DILEPTON.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("OUT_Yt_DILEPTON.root");
      }
      f->GetObject("nominal",tree);

   }
   Init(tree);
}

Yt_noKLFitter::~Yt_noKLFitter()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Yt_noKLFitter::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Yt_noKLFitter::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Yt_noKLFitter::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   t_names_mc_generator_weights = 0;
   temp_totalEventsWeighted_mc_generator_weights = 0;
   t_mc_generator_weights = 0;
   t_weight_bTagSF_DL1r_77_eigenvars_B_up = 0;
   t_weight_bTagSF_DL1r_77_eigenvars_C_up = 0;
   t_weight_bTagSF_DL1r_77_eigenvars_Light_up = 0;
   t_weight_bTagSF_DL1r_77_eigenvars_B_down = 0;
   t_weight_bTagSF_DL1r_77_eigenvars_C_down = 0;
   t_weight_bTagSF_DL1r_77_eigenvars_Light_down = 0;
   t_el_pt = 0;
   t_el_eta = 0;
   t_el_phi = 0;
   t_el_e = 0;
   t_mu_pt = 0;
   t_mu_eta = 0;
   t_mu_phi = 0;
   t_mu_e = 0;
   t_lep_d0sig = 0;
   t_jet_pt = 0;
   t_jet_eta = 0;
   t_jet_phi = 0;
   t_jet_m = 0;
   t_jet_jvt = 0;
   t_jet_isbtagged_DL1r_77 = 0;
   t_jet_DL1r = 0;
   truth_mc_pow_pt = 0;
   truth_mc_pow_eta = 0;
   truth_mc_pow_phi = 0;
   truth_mc_pow_e = 0;
   truth_mc_pow_pdgId = 0;
   truth_mc_pow_status = 0;
   truth_PDFinfo_PDGID1 = 0;
   truth_PDFinfo_PDGID2 = 0;
   t_matched_reco_PARTON_dRmin = 0;
   t_matched_reco_PARTON_recoJetIndex = 0;
   t_matched_reco_PARTON_PARTONJetIndex = 0;
   t_all_jet_reco_PARTON_Index_eachRecoJet = 0;
   t_PARTON_jetInd_dRmin = 0;
   t_PARTON_dRmin = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("xsec_feff_kfac", &xsec_feff_kfac, &b_xsec_feff_kfac);
   fChain->SetBranchAddress("t_weight_mc", &t_weight_mc, &b_t_weight_mc);
   fChain->SetBranchAddress("t_weight_pileup", &t_weight_pileup, &b_t_weight_pileup);
   fChain->SetBranchAddress("t_weight_leptonSF", &t_weight_leptonSF, &b_t_weight_leptonSF);
   fChain->SetBranchAddress("t_weight_globalLeptonTriggerSF", &t_weight_globalLeptonTriggerSF, &b_t_weight_globalLeptonTriggerSF);
   fChain->SetBranchAddress("t_weight_oldTriggerSF", &t_weight_oldTriggerSF, &b_t_weight_oldTriggerSF);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77", &t_weight_bTagSF_DL1r_77, &b_t_weight_bTagSF_DL1r_77);
   fChain->SetBranchAddress("t_weight_jvt", &t_weight_jvt, &b_t_weight_jvt);
   fChain->SetBranchAddress("t_names_mc_generator_weights", &t_names_mc_generator_weights, &b_t_names_mc_generator_weights);
   fChain->SetBranchAddress("temp_totalEventsWeighted_mc_generator_weights", &temp_totalEventsWeighted_mc_generator_weights, &b_temp_totalEventsWeighted_mc_generator_weights);
   fChain->SetBranchAddress("t_totalEventsWeighted", &t_totalEventsWeighted, &b_t_totalEventsWeighted);
   fChain->SetBranchAddress("t_mc_generator_weights", &t_mc_generator_weights, &b_t_mc_generator_weights);
   fChain->SetBranchAddress("t_weight_pileup_UP", &t_weight_pileup_UP, &b_t_weight_pileup_UP);
   fChain->SetBranchAddress("t_weight_pileup_DOWN", &t_weight_pileup_DOWN, &b_t_weight_pileup_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_Trigger_UP", &t_weight_leptonSF_EL_SF_Trigger_UP, &b_t_weight_leptonSF_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_Trigger_DOWN", &t_weight_leptonSF_EL_SF_Trigger_DOWN, &b_t_weight_leptonSF_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_Reco_UP", &t_weight_leptonSF_EL_SF_Reco_UP, &b_t_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_Reco_DOWN", &t_weight_leptonSF_EL_SF_Reco_DOWN, &b_t_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_ID_UP", &t_weight_leptonSF_EL_SF_ID_UP, &b_t_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_ID_DOWN", &t_weight_leptonSF_EL_SF_ID_DOWN, &b_t_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_Isol_UP", &t_weight_leptonSF_EL_SF_Isol_UP, &b_t_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_EL_SF_Isol_DOWN", &t_weight_leptonSF_EL_SF_Isol_DOWN, &b_t_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Trigger_STAT_UP", &t_weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_t_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &t_weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_t_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Trigger_SYST_UP", &t_weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_t_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &t_weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_t_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_STAT_UP", &t_weight_leptonSF_MU_SF_ID_STAT_UP, &b_t_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_STAT_DOWN", &t_weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_t_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_SYST_UP", &t_weight_leptonSF_MU_SF_ID_SYST_UP, &b_t_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_SYST_DOWN", &t_weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_t_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_t_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_t_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Isol_STAT_UP", &t_weight_leptonSF_MU_SF_Isol_STAT_UP, &b_t_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Isol_STAT_DOWN", &t_weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_t_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Isol_SYST_UP", &t_weight_leptonSF_MU_SF_Isol_SYST_UP, &b_t_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_Isol_SYST_DOWN", &t_weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_t_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_TTVA_STAT_UP", &t_weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_t_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &t_weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_t_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_TTVA_SYST_UP", &t_weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_t_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("t_weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &t_weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_t_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("t_weight_globalLeptonTriggerSF_EL_Trigger_UP", &t_weight_globalLeptonTriggerSF_EL_Trigger_UP, &b_t_weight_globalLeptonTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("t_weight_globalLeptonTriggerSF_EL_Trigger_DOWN", &t_weight_globalLeptonTriggerSF_EL_Trigger_DOWN, &b_t_weight_globalLeptonTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP", &t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP, &b_t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN", &t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN, &b_t_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP", &t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP, &b_t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN", &t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN, &b_t_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("t_weight_oldTriggerSF_EL_Trigger_UP", &t_weight_oldTriggerSF_EL_Trigger_UP, &b_t_weight_oldTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("t_weight_oldTriggerSF_EL_Trigger_DOWN", &t_weight_oldTriggerSF_EL_Trigger_DOWN, &b_t_weight_oldTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("t_weight_oldTriggerSF_MU_Trigger_STAT_UP", &t_weight_oldTriggerSF_MU_Trigger_STAT_UP, &b_t_weight_oldTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("t_weight_oldTriggerSF_MU_Trigger_STAT_DOWN", &t_weight_oldTriggerSF_MU_Trigger_STAT_DOWN, &b_t_weight_oldTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("t_weight_oldTriggerSF_MU_Trigger_SYST_UP", &t_weight_oldTriggerSF_MU_Trigger_SYST_UP, &b_t_weight_oldTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("t_weight_oldTriggerSF_MU_Trigger_SYST_DOWN", &t_weight_oldTriggerSF_MU_Trigger_SYST_DOWN, &b_t_weight_oldTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("t_weight_jvt_UP", &t_weight_jvt_UP, &b_t_weight_jvt_UP);
   fChain->SetBranchAddress("t_weight_jvt_DOWN", &t_weight_jvt_DOWN, &b_t_weight_jvt_DOWN);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_eigenvars_B_up", &t_weight_bTagSF_DL1r_77_eigenvars_B_up, &b_t_weight_bTagSF_DL1r_77_eigenvars_B_up);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_eigenvars_C_up", &t_weight_bTagSF_DL1r_77_eigenvars_C_up, &b_t_weight_bTagSF_DL1r_77_eigenvars_C_up);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_eigenvars_Light_up", &t_weight_bTagSF_DL1r_77_eigenvars_Light_up, &b_t_weight_bTagSF_DL1r_77_eigenvars_Light_up);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_eigenvars_B_down", &t_weight_bTagSF_DL1r_77_eigenvars_B_down, &b_t_weight_bTagSF_DL1r_77_eigenvars_B_down);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_eigenvars_C_down", &t_weight_bTagSF_DL1r_77_eigenvars_C_down, &b_t_weight_bTagSF_DL1r_77_eigenvars_C_down);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_eigenvars_Light_down", &t_weight_bTagSF_DL1r_77_eigenvars_Light_down, &b_t_weight_bTagSF_DL1r_77_eigenvars_Light_down);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_extrapolation_up", &t_weight_bTagSF_DL1r_77_extrapolation_up, &b_t_weight_bTagSF_DL1r_77_extrapolation_up);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_extrapolation_down", &t_weight_bTagSF_DL1r_77_extrapolation_down, &b_t_weight_bTagSF_DL1r_77_extrapolation_down);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_extrapolation_from_charm_up", &t_weight_bTagSF_DL1r_77_extrapolation_from_charm_up, &b_t_weight_bTagSF_DL1r_77_extrapolation_from_charm_up);
   fChain->SetBranchAddress("t_weight_bTagSF_DL1r_77_extrapolation_from_charm_down", &t_weight_bTagSF_DL1r_77_extrapolation_from_charm_down, &b_t_weight_bTagSF_DL1r_77_extrapolation_from_charm_down);
   fChain->SetBranchAddress("t_el_trigger_pass", &t_el_trigger_pass, &b_t_el_trigger_pass);
   fChain->SetBranchAddress("t_mu_trigger_pass", &t_mu_trigger_pass, &b_t_mu_trigger_pass);
   fChain->SetBranchAddress("t_eventNumber", &t_eventNumber, &b_t_eventNumber);
   fChain->SetBranchAddress("t_runNumber", &t_runNumber, &b_t_runNumber);
   fChain->SetBranchAddress("t_mcChannelNumber", &t_mcChannelNumber, &b_t_mcChannelNumber);
   fChain->SetBranchAddress("t_mu", &t_mu, &b_t_mu);
   fChain->SetBranchAddress("t_mu_actual", &t_mu_actual, &b_t_mu_actual);
   fChain->SetBranchAddress("t_n_el", &t_n_el, &b_t_n_el);
   fChain->SetBranchAddress("t_n_mu", &t_n_mu, &b_t_n_mu);
   fChain->SetBranchAddress("t_el_pt", &t_el_pt, &b_t_el_pt);
   fChain->SetBranchAddress("t_el_eta", &t_el_eta, &b_t_el_eta);
   fChain->SetBranchAddress("t_el_phi", &t_el_phi, &b_t_el_phi);
   fChain->SetBranchAddress("t_el_e", &t_el_e, &b_t_el_e);
   fChain->SetBranchAddress("t_mu_pt", &t_mu_pt, &b_t_mu_pt);
   fChain->SetBranchAddress("t_mu_eta", &t_mu_eta, &b_t_mu_eta);
   fChain->SetBranchAddress("t_mu_phi", &t_mu_phi, &b_t_mu_phi);
   fChain->SetBranchAddress("t_mu_e", &t_mu_e, &b_t_mu_e);
   fChain->SetBranchAddress("t_lep_d0sig", &t_lep_d0sig, &b_t_lep_d0sig);
   fChain->SetBranchAddress("t_Goodjet_num", &t_Goodjet_num, &b_t_Goodjet_num);
   fChain->SetBranchAddress("t_jet_num", &t_jet_num, &b_t_jet_num);
   fChain->SetBranchAddress("t_bjet_num", &t_bjet_num, &b_t_bjet_num);
   fChain->SetBranchAddress("t_jet_pt", &t_jet_pt, &b_t_jet_pt);
   fChain->SetBranchAddress("t_jet_eta", &t_jet_eta, &b_t_jet_eta);
   fChain->SetBranchAddress("t_jet_phi", &t_jet_phi, &b_t_jet_phi);
   fChain->SetBranchAddress("t_jet_m", &t_jet_m, &b_t_jet_m);
   fChain->SetBranchAddress("t_jet_jvt", &t_jet_jvt, &b_t_jet_jvt);
   fChain->SetBranchAddress("t_jet_isbtagged_DL1r_77", &t_jet_isbtagged_DL1r_77, &b_t_jet_isbtagged_DL1r_77);
   fChain->SetBranchAddress("t_jet_DL1r", &t_jet_DL1r, &b_t_jet_DL1r);
   fChain->SetBranchAddress("t_met_met", &t_met_met, &b_t_met_met);
   fChain->SetBranchAddress("t_met_phi", &t_met_phi, &b_t_met_phi);
   fChain->SetBranchAddress("t_ejets_DL1r_2015", &t_ejets_DL1r_2015, &b_t_ejets_DL1r_2015);
   fChain->SetBranchAddress("t_ejets_DL1r_2016", &t_ejets_DL1r_2016, &b_t_ejets_DL1r_2016);
   fChain->SetBranchAddress("t_ejets_DL1r_2017", &t_ejets_DL1r_2017, &b_t_ejets_DL1r_2017);
   fChain->SetBranchAddress("t_ejets_DL1r_2018", &t_ejets_DL1r_2018, &b_t_ejets_DL1r_2018);
   fChain->SetBranchAddress("t_mujets_DL1r_2015", &t_mujets_DL1r_2015, &b_t_mujets_DL1r_2015);
   fChain->SetBranchAddress("t_mujets_DL1r_2016", &t_mujets_DL1r_2016, &b_t_mujets_DL1r_2016);
   fChain->SetBranchAddress("t_mujets_DL1r_2017", &t_mujets_DL1r_2017, &b_t_mujets_DL1r_2017);
   fChain->SetBranchAddress("t_mujets_DL1r_2018", &t_mujets_DL1r_2018, &b_t_mujets_DL1r_2018);
   fChain->SetBranchAddress("t_ejets_particle", &t_ejets_particle, &b_t_ejets_particle);
   fChain->SetBranchAddress("t_mujets_particle", &t_mujets_particle, &b_t_mujets_particle);
   fChain->SetBranchAddress("t_reco_lj1_pt", &t_reco_lj1_pt, &b_t_reco_lj1_pt);
   fChain->SetBranchAddress("t_reco_lj1_eta", &t_reco_lj1_eta, &b_t_reco_lj1_eta);
   fChain->SetBranchAddress("t_reco_lj1_phi", &t_reco_lj1_phi, &b_t_reco_lj1_phi);
   fChain->SetBranchAddress("t_reco_lj1_e", &t_reco_lj1_e, &b_t_reco_lj1_e);
   fChain->SetBranchAddress("t_reco_lj2_pt", &t_reco_lj2_pt, &b_t_reco_lj2_pt);
   fChain->SetBranchAddress("t_reco_lj2_eta", &t_reco_lj2_eta, &b_t_reco_lj2_eta);
   fChain->SetBranchAddress("t_reco_lj2_phi", &t_reco_lj2_phi, &b_t_reco_lj2_phi);
   fChain->SetBranchAddress("t_reco_lj2_e", &t_reco_lj2_e, &b_t_reco_lj2_e);
   fChain->SetBranchAddress("t_reco_Whad_pt", &t_reco_Whad_pt, &b_t_reco_Whad_pt);
   fChain->SetBranchAddress("t_reco_Whad_eta", &t_reco_Whad_eta, &b_t_reco_Whad_eta);
   fChain->SetBranchAddress("t_reco_Whad_phi", &t_reco_Whad_phi, &b_t_reco_Whad_phi);
   fChain->SetBranchAddress("t_reco_Whad_m", &t_reco_Whad_m, &b_t_reco_Whad_m);
   fChain->SetBranchAddress("t_reco_bhad_pt", &t_reco_bhad_pt, &b_t_reco_bhad_pt);
   fChain->SetBranchAddress("t_reco_bhad_eta", &t_reco_bhad_eta, &b_t_reco_bhad_eta);
   fChain->SetBranchAddress("t_reco_bhad_phi", &t_reco_bhad_phi, &b_t_reco_bhad_phi);
   fChain->SetBranchAddress("t_reco_bhad_m", &t_reco_bhad_m, &b_t_reco_bhad_m);
   fChain->SetBranchAddress("t_reco_thad_pt", &t_reco_thad_pt, &b_t_reco_thad_pt);
   fChain->SetBranchAddress("t_reco_thad_eta", &t_reco_thad_eta, &b_t_reco_thad_eta);
   fChain->SetBranchAddress("t_reco_thad_phi", &t_reco_thad_phi, &b_t_reco_thad_phi);
   fChain->SetBranchAddress("t_reco_thad_e", &t_reco_thad_e, &b_t_reco_thad_e);
   fChain->SetBranchAddress("t_reco_thad_m", &t_reco_thad_m, &b_t_reco_thad_m);
   fChain->SetBranchAddress("t_reco_nu_pt", &t_reco_nu_pt, &b_t_reco_nu_pt);
   fChain->SetBranchAddress("t_reco_nu_eta", &t_reco_nu_eta, &b_t_reco_nu_eta);
   fChain->SetBranchAddress("t_reco_nu_phi", &t_reco_nu_phi, &b_t_reco_nu_phi);
   fChain->SetBranchAddress("t_reco_nu_e", &t_reco_nu_e, &b_t_reco_nu_e);
   fChain->SetBranchAddress("t_reco_nu_pt_z", &t_reco_nu_pt_z, &b_t_reco_nu_pt_z);
   fChain->SetBranchAddress("t_delta_neg", &t_delta_neg, &b_t_delta_neg);
   fChain->SetBranchAddress("t_delta_neg_resc", &t_delta_neg_resc, &b_t_delta_neg_resc);
   fChain->SetBranchAddress("t_delta_neg_4", &t_delta_neg_4, &b_t_delta_neg_4);
   fChain->SetBranchAddress("t_delta_neg_resc_4", &t_delta_neg_resc_4, &b_t_delta_neg_resc_4);
   fChain->SetBranchAddress("t_reco_lep_pt", &t_reco_lep_pt, &b_t_reco_lep_pt);
   fChain->SetBranchAddress("t_reco_lep_eta", &t_reco_lep_eta, &b_t_reco_lep_eta);
   fChain->SetBranchAddress("t_reco_lep_phi", &t_reco_lep_phi, &b_t_reco_lep_phi);
   fChain->SetBranchAddress("t_reco_lep_e", &t_reco_lep_e, &b_t_reco_lep_e);
   fChain->SetBranchAddress("t_reco_Wlep_pt", &t_reco_Wlep_pt, &b_t_reco_Wlep_pt);
   fChain->SetBranchAddress("t_reco_Wlep_eta", &t_reco_Wlep_eta, &b_t_reco_Wlep_eta);
   fChain->SetBranchAddress("t_reco_Wlep_phi", &t_reco_Wlep_phi, &b_t_reco_Wlep_phi);
   fChain->SetBranchAddress("t_reco_Wlep_m", &t_reco_Wlep_m, &b_t_reco_Wlep_m);
   fChain->SetBranchAddress("t_reco_Wlep_mT", &t_reco_Wlep_mT, &b_t_reco_Wlep_mT);
   fChain->SetBranchAddress("t_reco_Wlep_m_4vec", &t_reco_Wlep_m_4vec, &b_t_reco_Wlep_m_4vec);
   fChain->SetBranchAddress("t_reco_blep_pt", &t_reco_blep_pt, &b_t_reco_blep_pt);
   fChain->SetBranchAddress("t_reco_blep_eta", &t_reco_blep_eta, &b_t_reco_blep_eta);
   fChain->SetBranchAddress("t_reco_blep_phi", &t_reco_blep_phi, &b_t_reco_blep_phi);
   fChain->SetBranchAddress("t_reco_blep_m", &t_reco_blep_m, &b_t_reco_blep_m);
   fChain->SetBranchAddress("t_reco_tlep_pt", &t_reco_tlep_pt, &b_t_reco_tlep_pt);
   fChain->SetBranchAddress("t_reco_tlep_eta", &t_reco_tlep_eta, &b_t_reco_tlep_eta);
   fChain->SetBranchAddress("t_reco_tlep_phi", &t_reco_tlep_phi, &b_t_reco_tlep_phi);
   fChain->SetBranchAddress("t_reco_tlep_e", &t_reco_tlep_e, &b_t_reco_tlep_e);
   fChain->SetBranchAddress("t_reco_tlep_m", &t_reco_tlep_m, &b_t_reco_tlep_m);
   fChain->SetBranchAddress("t_reco_ttbar_pt", &t_reco_ttbar_pt, &b_t_reco_ttbar_pt);
   fChain->SetBranchAddress("t_reco_ttbar_eta", &t_reco_ttbar_eta, &b_t_reco_ttbar_eta);
   fChain->SetBranchAddress("t_reco_ttbar_phi", &t_reco_ttbar_phi, &b_t_reco_ttbar_phi);
   fChain->SetBranchAddress("t_reco_ttbar_e", &t_reco_ttbar_e, &b_t_reco_ttbar_e);
   fChain->SetBranchAddress("t_reco_ttbar_m", &t_reco_ttbar_m, &b_t_reco_ttbar_m);
   fChain->SetBranchAddress("t_reco_lj1_resc_pt", &t_reco_lj1_resc_pt, &b_t_reco_lj1_resc_pt);
   fChain->SetBranchAddress("t_reco_lj1_resc_eta", &t_reco_lj1_resc_eta, &b_t_reco_lj1_resc_eta);
   fChain->SetBranchAddress("t_reco_lj1_resc_phi", &t_reco_lj1_resc_phi, &b_t_reco_lj1_resc_phi);
   fChain->SetBranchAddress("t_reco_lj1_resc_e", &t_reco_lj1_resc_e, &b_t_reco_lj1_resc_e);
   fChain->SetBranchAddress("t_reco_lj2_resc_pt", &t_reco_lj2_resc_pt, &b_t_reco_lj2_resc_pt);
   fChain->SetBranchAddress("t_reco_lj2_resc_eta", &t_reco_lj2_resc_eta, &b_t_reco_lj2_resc_eta);
   fChain->SetBranchAddress("t_reco_lj2_resc_phi", &t_reco_lj2_resc_phi, &b_t_reco_lj2_resc_phi);
   fChain->SetBranchAddress("t_reco_lj2_resc_e", &t_reco_lj2_resc_e, &b_t_reco_lj2_resc_e);
   fChain->SetBranchAddress("t_reco_Whad_resc_pt", &t_reco_Whad_resc_pt, &b_t_reco_Whad_resc_pt);
   fChain->SetBranchAddress("t_reco_Whad_resc_eta", &t_reco_Whad_resc_eta, &b_t_reco_Whad_resc_eta);
   fChain->SetBranchAddress("t_reco_Whad_resc_phi", &t_reco_Whad_resc_phi, &b_t_reco_Whad_resc_phi);
   fChain->SetBranchAddress("t_reco_Whad_resc_m", &t_reco_Whad_resc_m, &b_t_reco_Whad_resc_m);
   fChain->SetBranchAddress("t_reco_bhad_resc_pt", &t_reco_bhad_resc_pt, &b_t_reco_bhad_resc_pt);
   fChain->SetBranchAddress("t_reco_bhad_resc_eta", &t_reco_bhad_resc_eta, &b_t_reco_bhad_resc_eta);
   fChain->SetBranchAddress("t_reco_bhad_resc_phi", &t_reco_bhad_resc_phi, &b_t_reco_bhad_resc_phi);
   fChain->SetBranchAddress("t_reco_bhad_resc_m", &t_reco_bhad_resc_m, &b_t_reco_bhad_resc_m);
   fChain->SetBranchAddress("t_reco_thad_resc_pt", &t_reco_thad_resc_pt, &b_t_reco_thad_resc_pt);
   fChain->SetBranchAddress("t_reco_thad_resc_eta", &t_reco_thad_resc_eta, &b_t_reco_thad_resc_eta);
   fChain->SetBranchAddress("t_reco_thad_resc_phi", &t_reco_thad_resc_phi, &b_t_reco_thad_resc_phi);
   fChain->SetBranchAddress("t_reco_thad_resc_e", &t_reco_thad_resc_e, &b_t_reco_thad_resc_e);
   fChain->SetBranchAddress("t_reco_thad_resc_m", &t_reco_thad_resc_m, &b_t_reco_thad_resc_m);
   fChain->SetBranchAddress("t_reco_nu_resc_pt", &t_reco_nu_resc_pt, &b_t_reco_nu_resc_pt);
   fChain->SetBranchAddress("t_reco_nu_resc_eta", &t_reco_nu_resc_eta, &b_t_reco_nu_resc_eta);
   fChain->SetBranchAddress("t_reco_nu_resc_phi", &t_reco_nu_resc_phi, &b_t_reco_nu_resc_phi);
   fChain->SetBranchAddress("t_reco_nu_resc_e", &t_reco_nu_resc_e, &b_t_reco_nu_resc_e);
   fChain->SetBranchAddress("t_reco_lep_resc_pt", &t_reco_lep_resc_pt, &b_t_reco_lep_resc_pt);
   fChain->SetBranchAddress("t_reco_lep_resc_eta", &t_reco_lep_resc_eta, &b_t_reco_lep_resc_eta);
   fChain->SetBranchAddress("t_reco_lep_resc_phi", &t_reco_lep_resc_phi, &b_t_reco_lep_resc_phi);
   fChain->SetBranchAddress("t_reco_lep_resc_e", &t_reco_lep_resc_e, &b_t_reco_lep_resc_e);
   fChain->SetBranchAddress("t_reco_Wlep_resc_pt", &t_reco_Wlep_resc_pt, &b_t_reco_Wlep_resc_pt);
   fChain->SetBranchAddress("t_reco_Wlep_resc_eta", &t_reco_Wlep_resc_eta, &b_t_reco_Wlep_resc_eta);
   fChain->SetBranchAddress("t_reco_Wlep_resc_phi", &t_reco_Wlep_resc_phi, &b_t_reco_Wlep_resc_phi);
   fChain->SetBranchAddress("t_reco_Wlep_resc_m", &t_reco_Wlep_resc_m, &b_t_reco_Wlep_resc_m);
   fChain->SetBranchAddress("t_reco_Wlep_resc_mT", &t_reco_Wlep_resc_mT, &b_t_reco_Wlep_resc_mT);
   fChain->SetBranchAddress("t_reco_blep_resc_pt", &t_reco_blep_resc_pt, &b_t_reco_blep_resc_pt);
   fChain->SetBranchAddress("t_reco_blep_resc_eta", &t_reco_blep_resc_eta, &b_t_reco_blep_resc_eta);
   fChain->SetBranchAddress("t_reco_blep_resc_phi", &t_reco_blep_resc_phi, &b_t_reco_blep_resc_phi);
   fChain->SetBranchAddress("t_reco_blep_resc_m", &t_reco_blep_resc_m, &b_t_reco_blep_resc_m);
   fChain->SetBranchAddress("t_reco_tlep_resc_pt", &t_reco_tlep_resc_pt, &b_t_reco_tlep_resc_pt);
   fChain->SetBranchAddress("t_reco_tlep_resc_eta", &t_reco_tlep_resc_eta, &b_t_reco_tlep_resc_eta);
   fChain->SetBranchAddress("t_reco_tlep_resc_phi", &t_reco_tlep_resc_phi, &b_t_reco_tlep_resc_phi);
   fChain->SetBranchAddress("t_reco_tlep_resc_e", &t_reco_tlep_resc_e, &b_t_reco_tlep_resc_e);
   fChain->SetBranchAddress("t_reco_tlep_resc_m", &t_reco_tlep_resc_m, &b_t_reco_tlep_resc_m);
   fChain->SetBranchAddress("t_reco_ttbar_resc_pt", &t_reco_ttbar_resc_pt, &b_t_reco_ttbar_resc_pt);
   fChain->SetBranchAddress("t_reco_ttbar_resc_eta", &t_reco_ttbar_resc_eta, &b_t_reco_ttbar_resc_eta);
   fChain->SetBranchAddress("t_reco_ttbar_resc_phi", &t_reco_ttbar_resc_phi, &b_t_reco_ttbar_resc_phi);
   fChain->SetBranchAddress("t_reco_ttbar_resc_e", &t_reco_ttbar_resc_e, &b_t_reco_ttbar_resc_e);
   fChain->SetBranchAddress("t_reco_ttbar_resc_m", &t_reco_ttbar_resc_m, &b_t_reco_ttbar_resc_m);
   fChain->SetBranchAddress("t_HT_jets", &t_HT_jets, &b_t_HT_jets);
   fChain->SetBranchAddress("t_ST_jets_ETmiss_lep", &t_ST_jets_ETmiss_lep, &b_t_ST_jets_ETmiss_lep);
   fChain->SetBranchAddress("truth_mc_pow_pt", &truth_mc_pow_pt, &b_truth_mc_pow_pt);
   fChain->SetBranchAddress("truth_mc_pow_eta", &truth_mc_pow_eta, &b_truth_mc_pow_eta);
   fChain->SetBranchAddress("truth_mc_pow_phi", &truth_mc_pow_phi, &b_truth_mc_pow_phi);
   fChain->SetBranchAddress("truth_mc_pow_e", &truth_mc_pow_e, &b_truth_mc_pow_e);
   fChain->SetBranchAddress("truth_mc_pow_pdgId", &truth_mc_pow_pdgId, &b_truth_mc_pow_pdgId);
   fChain->SetBranchAddress("truth_mc_pow_status", &truth_mc_pow_status, &b_truth_mc_pow_status);
   fChain->SetBranchAddress("truth_PDFinfo_PDGID1", &truth_PDFinfo_PDGID1, &b_truth_PDFinfo_PDGID1);
   fChain->SetBranchAddress("truth_PDFinfo_PDGID2", &truth_PDFinfo_PDGID2, &b_truth_PDFinfo_PDGID2);
   fChain->SetBranchAddress("t_matched_reco_PARTON_dRmin", &t_matched_reco_PARTON_dRmin, &b_t_matched_reco_PARTON_dRmin);
   fChain->SetBranchAddress("t_matched_reco_PARTON_recoJetIndex", &t_matched_reco_PARTON_recoJetIndex, &b_t_matched_reco_PARTON_recoJetIndex);
   fChain->SetBranchAddress("t_matched_reco_PARTON_PARTONJetIndex", &t_matched_reco_PARTON_PARTONJetIndex, &b_t_matched_reco_PARTON_PARTONJetIndex);
   fChain->SetBranchAddress("t_PARTONmatchedJets_TotalNum", &t_PARTONmatchedJets_TotalNum, &b_t_PARTONmatchedJets_TotalNum);
   fChain->SetBranchAddress("t_all_jet_reco_PARTON_Index_eachRecoJet", &t_all_jet_reco_PARTON_Index_eachRecoJet, &b_t_all_jet_reco_PARTON_Index_eachRecoJet);
   fChain->SetBranchAddress("t_isPerfectMatch_PARTONLevel", &t_isPerfectMatch_PARTONLevel, &b_t_isPerfectMatch_PARTONLevel);
   fChain->SetBranchAddress("t_partonLev_nu_met", &t_partonLev_nu_met, &b_t_partonLev_nu_met);
   fChain->SetBranchAddress("t_partonLev_nu_eta", &t_partonLev_nu_eta, &b_t_partonLev_nu_eta);
   fChain->SetBranchAddress("t_partonLev_nu_phi", &t_partonLev_nu_phi, &b_t_partonLev_nu_phi);
   fChain->SetBranchAddress("t_partonLev_nu_m", &t_partonLev_nu_m, &b_t_partonLev_nu_m);
   fChain->SetBranchAddress("t_reco_Wjet1_ind", &t_reco_Wjet1_ind, &b_t_reco_Wjet1_ind);
   fChain->SetBranchAddress("t_reco_Wjet2_ind", &t_reco_Wjet2_ind, &b_t_reco_Wjet2_ind);
   fChain->SetBranchAddress("t_reco_bhad_ind", &t_reco_bhad_ind, &b_t_reco_bhad_ind);
   fChain->SetBranchAddress("t_reco_blep_ind", &t_reco_blep_ind, &b_t_reco_blep_ind);
   fChain->SetBranchAddress("t_PARTON_jetInd_dRmin", &t_PARTON_jetInd_dRmin, &b_t_PARTON_jetInd_dRmin);
   fChain->SetBranchAddress("t_PARTON_dRmin", &t_PARTON_dRmin, &b_t_PARTON_dRmin);
   fChain->SetBranchAddress("t_noPARTONMatch", &t_noPARTONMatch, &b_t_noPARTONMatch);
   fChain->SetBranchAddress("t_truth_MC_ttbar_afterFSR_beforeDecay_m", &t_truth_MC_ttbar_afterFSR_beforeDecay_m, &b_t_truth_MC_ttbar_afterFSR_beforeDecay_m);
   fChain->SetBranchAddress("t_weight_hathor_ytsqm9", &t_weight_hathor_ytsqm9, &b_t_weight_hathor_ytsqm9);
   fChain->SetBranchAddress("t_weight_hathor_ytsqm4", &t_weight_hathor_ytsqm4, &b_t_weight_hathor_ytsqm4);
   fChain->SetBranchAddress("t_weight_hathor_ytsqm1", &t_weight_hathor_ytsqm1, &b_t_weight_hathor_ytsqm1);
   fChain->SetBranchAddress("t_weight_hathor_ytsq0", &t_weight_hathor_ytsq0, &b_t_weight_hathor_ytsq0);
   fChain->SetBranchAddress("t_weight_hathor_ytsq1", &t_weight_hathor_ytsq1, &b_t_weight_hathor_ytsq1);
   fChain->SetBranchAddress("t_weight_hathor_ytsq4", &t_weight_hathor_ytsq4, &b_t_weight_hathor_ytsq4);
   fChain->SetBranchAddress("t_weight_hathor_ytsq9", &t_weight_hathor_ytsq9, &b_t_weight_hathor_ytsq9);
   fChain->SetBranchAddress("t_weight_hathor_ytsq16", &t_weight_hathor_ytsq16, &b_t_weight_hathor_ytsq16);
   Notify();
}

Bool_t Yt_noKLFitter::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Yt_noKLFitter::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Yt_noKLFitter::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Yt_noKLFitter_cxx
